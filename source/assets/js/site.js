var Site = (function($, window, undefined) {
  'use strict';

  var privateVar = null;
  var privateMethod = function() {
    $(document).ready(function() {
      $('.highlight-product .list-items').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        // {
        //   breakpoint: 480,
        //   settings: {
        //     slidesToShow: 1
        //   }
        // }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
      });
    });
  };

  return {
    publicVar: privateVar,
    publicMethod: privateMethod
  };

})(jQuery, window);

jQuery(function() {
  Site.publicMethod();
});

$(function(){
$('.icon-menu').click(function(){
    if($(this).parent().find('.block-header').css('display')=="none"){
      $('.block-header').fadeIn(400);
      $(this).addClass('active');
      $('.logo-mb').hide()
    }
    else
    {
      $(this).parent().find('.block-header').fadeOut(400);
      $(this).removeClass('active');
      $('.logo-mb').show()
    }
  }); 

var scwidth = $(window).width()
var scheight = $(window).height()

  
  if (scwidth <=768) {
    $('.main-menu ul li:has(ul)').addClass('sub-1')
    $('.main-menu .sub-1 > a').attr('href','javascript:void(0)') 
    $('.main-menu .sub-1 > a').click(function(){
    if($(this).parent().find('> ul').css('display')=="none"){
       $('.main-menu ul ul').slideUp()
      $(this).parent().find('> ul').slideDown(400);
      $(this).addClass('active');
    }
    else
    {
      $(this).parent().find('> ul').slideUp(400);
      $(this).removeClass('active');
    }
  }); 
  $('.block-header').css({'min-height':scheight})
  };
  $('#form-search').appendTo('.block-header')
  $('#form-search').click(function(){
    $(this).addClass('form-search-active')
  });

})